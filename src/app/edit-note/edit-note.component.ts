import { Component, OnInit } from '@angular/core';
import { Note } from '../note';
import { DataService } from '../data.service';
import {ActivatedRoute, Router} from '@angular/router'

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent implements OnInit {

  currentNoteId;
  noteModel = new Note('Tytuł', 'Treść');

  constructor(private data: DataService, private route: ActivatedRoute , public router: Router) {
      this.route.params.subscribe( params => this.currentNoteId = params.id);
      console.log(this.currentNoteId);
  }

  onSubmit(){
    this.data.editNote(this.noteModel, this.currentNoteId).subscribe(
      ()=>{
        this.router.navigate(['/']);
      }
    )
  }
  onDelete(){
    this.data.deleteNote(this.currentNoteId).subscribe(
      ()=> {
        this.router.navigate(['/']);
      }
    )
  }
  ngOnInit(){
    this.data.getNoteSingle(this.currentNoteId).subscribe(
      data => {
        console.log(data);
        this.noteModel.title = data["title"];
        this.noteModel.content = data["content"];
      }
    )   
  }
}
