import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import {ActivatedRoute, Router} from '@angular/router'

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {

  notes$: Object;

  constructor(private data: DataService, public router: Router) {}
  haveNotes :boolean = false;


  getNotes(){
    this.data.getNotesList().subscribe(
      data => {
        this.haveNotes = Object.keys(data).length != 0 ? true : false;
        this.notes$ = data
      }
    )
  }
  onDelete(id){
    this.data.deleteNote(id).subscribe(
      ()=> {
        console.log(id,"deleted");
        this.getNotes();
      }
    )
  }
  newNote(){
    this.router.navigate(['/new']);
  }
  ngOnInit() {
    this.getNotes();
  }

}
