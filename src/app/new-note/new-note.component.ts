import { Component, OnInit } from '@angular/core';
import { Note } from '../note';
import { DataService } from '../data.service';
import {ActivatedRoute, Router} from '@angular/router'

@Component({
  selector: 'app-new-note',
  templateUrl: './new-note.component.html',
  styleUrls: ['./new-note.component.scss']
})
export class NewNoteComponent{
  currentNoteId;
  noteModel = new Note('Tytuł', 'Treść');

  constructor(private data: DataService, private route: ActivatedRoute , public router: Router) {
      this.route.params.subscribe( params => this.currentNoteId = params.id);
      console.log(this.currentNoteId);
  }

  onSubmit(){
    console.log(this.noteModel);
    this.data.addNote(this.noteModel).subscribe(
      ()=>{
        this.router.navigate(['/']);
      }
    )
    
  }
}

