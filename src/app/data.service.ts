import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import {Note} from './note'

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http:HttpClient) {}
  public update(){
    this.getNotesList();
  }

  getNotesList(){
    return this.http.get('http://kazekfajny.pythonanywhere.com/note')
    }
  getNoteSingle(id){
    return this.http.get('http://kazekfajny.pythonanywhere.com/note/'+id)
    }
  deleteNote(id){
    return this.http.delete('http://kazekfajny.pythonanywhere.com/note/' +id)
  }
  addNote(note: Note){
    return this.http.post('http://kazekfajny.pythonanywhere.com/note', note)
  }
  editNote(note: Note, noteID){
    return this.http.put('http://kazekfajny.pythonanywhere.com/note/'+noteID, note);
  }
}
